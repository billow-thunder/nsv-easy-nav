# Nativescript Vue - Easy Nav

First off this is not a replacement for VueRouter in NSV. A true router for NSV is still a ways off.

Easy Nav is a simple wrapper for the built-in $navigateTo() functionality. This removes the need to import the component everytime you want to navigate to it and also allows you to interact with your routing layer in a more VueRouter like way.

### Installation

```javascript
npm i @billow/nsv-easy-nav
```

### Setup

```javascript
import Router from '@billow/nsv-easy-nav'

let routes = new Router({
  routes: [
    {
      name: 'welcome',
      component: Welcome
    }
  ]
})

// When the application comes out of a closed state, we need to determine which component 
// to render. If the user is not authenticated show the Welcome component and visa versa.
//
// Note: userIsAuthenticated is only an example, your application must handle authentication. 

let StartUpComponent = (userIsAuthenticated) ? Dashboard : Welcome

new Vue({

  router,
  
  render: h => h('frame', [h(StartUpComponent)]),

}).$start()
```

### Usage

```html
<StackLayout>
  <Button @tap="$router.go('dashboard')">Dashboard</Button> // Navigates with history
  <Button @tap="$router.clear.go('dashboard')">Dashboard</Button> // Navigates and clears back stack.
  <Button @tap="$router.go('user-edit', {
    userId: 1
  })">Edit User</Button> // Props passed as second argument
  <Button @tap="$router.back()">Back</Button> // Navigates with back stack
</StackLayout>
```

Usage within instance.

```javascript

export default {

  methods: {
    goToDashboard() {
      this.$router.go('dashboard') // Navigates with history
    },
    
    editUser(userId) {
      this.$router.go('user-edit', { // Navigates with a prop passed as second argument
        userId
      })
    },
  }

}
```

**Go**

The go method accepts 3 arguments with a few defaults for Android.

```javascript
  go(name, props = {}, transitions = {
    android: {
      name: 'slide',
      curve: enums.AnimationCurve.ease
    }
  })
```

**Clear**

Typically paired with `go()`, this method accepts no arguements. 

```javascript
  this.$router.clear.go()
```