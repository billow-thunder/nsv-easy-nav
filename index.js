import { install } from './src/install'
import { find } from 'lodash'
import Vue from 'nativescript-vue'
var enums = require("tns-core-modules/ui/enums");

export default class Router {

  constructor(options = {}) {

    if (!options.routes) {
      console.log('ROUTER: Missing required array of routes')
      return
    }

    this.routes = options.routes
    this.resetOptionsQueue()

    Vue.use(install)
  }

  init(app) {
    this.app = app
  }

  resetOptionsQueue() {
    this.optionsQueue = {}
  }

  get clear() {
    this.optionsQueue.clearHistory = true
    return this
  }

  go(name, props = {}, transitions = {
    android: {
      name: 'slide',
      curve: enums.AnimationCurve.ease
    }
  }) {
    let route = find(this.routes, route => route.name === name)

    if (!route) {
      console.log(`ROUTER: Route with name: ${name}, not found.`)
      return
    }

    this.app.$navigateTo(route.component, {
      props,
      transition: transitions.all,
      transitionIOS: transitions.ios,
      transitionAndroid: transitions.android,
      clearHistory: this.optionsQueue.clearHistory || false
    })

    this.resetOptionsQueue()
  }

  back() {
    this.app.$navigateBack()
  }

}
